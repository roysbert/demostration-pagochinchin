# Demostración de aptitudes #

Deberá elaborar una app Android con las siguientes características:

1- La aplicación deberá almacenar los valores token de seguridad y nombre de usuario al realizar el login. En caso de no disponer de un API para inicio de sesión, deberá usar unos valores estáticos.

2- Luego de iniciar sesión debe haber una vista para calcular monto de intercambio entre monedas, dicha vista deberá tener un cuadro de texto donde el usuario ingresará el monto a calcular y la moneda (Ej: 10.000 y moneda USD). Al presionar el botón calcular, debe llamar la API para obtener las tasas de cambio de las monedas disponibles (ETH, BTC, PTR, BS, EURO y USD). Con el resultado de dicha API hacer el cálculo del monto de todas las monedas disponibles. (En caso de que el api no responda o no consiga una api apropiada, crear unos valores estáticos). Posterior a hacer el cálculo deberá mostrar una vista con el resultado y las tasas de cambio usadas, asi como un QR con los valores arrojados.

3 - Deberá crear una vista que permita leer el QR anterior y mostrar tanto el monto inicial como el resultado en las monedas disponibles y una opción que sea recalcular y seguir el flujo de las vista anterior.

4- Deberá tener un menú donde puedan acceder a las 2 opciones y una tercera opción que será cerrar sesión.

5- El diseño de la APP queda de parte del participante.

6- Se tomará en cuenta todos los mensajes de alerta o errores de comunicación con los api (web o estáticos), así como que se valora el correcto flujo de la APP.

## Información:
- En caso de no conectar con el API del PTR, el valor fijo del mismo será de 1 PTR = 60 USD.
- En caso de no conectar con el API de BS, el valor fijo del mismo será de 100.000 BS = 1 USD.
- Puede usar cualquier REST API (JSON) para calcular el precio de las criptomonedas.(ejemplo API: https://www.binance.com/exchange-api/v1/public/asset-service/product/get-products de la página https://www.binance.com/es/markets).
- El código deberá subirse en un repositorio público de Git de su preferencia y compartir la URL (Se valorará las buenas prácticas y el correcto uso de la herramienta Git)
- Enviar el APK

Se valorará el buen uso del framework, así como la organización y optimización del código usado.
Todos los ítems extras incorporados por el participante se tomarán en cuenta.
