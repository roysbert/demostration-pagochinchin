package com.example.demostrationpagochinchin.ui.logout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;

import com.example.demostrationpagochinchin.HandlerApplication;
import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.ui.BaseFragment;

public class LogoutFragment extends BaseFragment implements View.OnClickListener {

    private Button buttonConfirm;
    private Button buttonDenied;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_logout, container, false);

        buttonConfirm = root.findViewById(R.id.logout_confirm);
        buttonConfirm.setOnClickListener(this);

        buttonDenied = root.findViewById(R.id.logout_cancel);
        buttonDenied.setOnClickListener(this);

        MainActivity().hideBottomNavigationBar();
        return root;
    }

    @Override
    public void onClick(View view) {
        if (view == buttonConfirm){
            HandlerApplication.shared(MainActivity()).logout();
            MainActivity().reload();
        } else if (view == buttonDenied){
            MainActivity().goBack();
        }
    }
}