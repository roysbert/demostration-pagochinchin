package com.example.demostrationpagochinchin.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.ActionCallback;
import com.example.demostrationpagochinchin.core.HandlerException;
import com.example.demostrationpagochinchin.core.Utils;
import com.example.demostrationpagochinchin.core.ValidOperation;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.ui.BaseFragment;

import java.util.Arrays;
import java.util.List;

public class HomeFragment extends BaseFragment implements ActionCallback, View.OnClickListener{

    private HomeViewModel homeViewModel;
    private View root;
    private EditText currencyAmount;
    private Spinner currencySpinner;
    private Button calculateButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);

        currencyAmount = root.findViewById(R.id.home_amount);
        currencySpinner = root.findViewById(R.id.home_currency_spinner);

        calculateButton = root.findViewById(R.id.home_calculate_button);
        calculateButton.setOnClickListener(this);

        final View homeLoading = root.findViewById(R.id.home_loading);
        final View homeContent = root.findViewById(R.id.home_content);
        homeViewModel.isLoading().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean value) {
                if (value){
                    homeLoading.setVisibility(View.VISIBLE);
                    homeContent.setVisibility(View.GONE);
                } else {
                    homeLoading.setVisibility(View.GONE);
                    homeContent.setVisibility(View.VISIBLE);
                }
            }
        });

        prepareSpinner();

        return root;
    }

    private void prepareSpinner() {
        List<Currency.Identifier> currencies = Arrays.asList(new Currency.Identifier[]{
                Currency.Identifier.USD,
                Currency.Identifier.EURO,
                Currency.Identifier.BTC,
                Currency.Identifier.ETH,
                Currency.Identifier.BS,
                Currency.Identifier.PTR
        });
        Spinner spinner = root.findViewById(R.id.home_currency_spinner);
        ArrayAdapter<Currency.Identifier> spinnerArrayAdapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, currencies);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onSuccessResponse(ValidOperation operation) {
        switch (operation){
            case BinanceCurrencies:
                homeViewModel.setLoading(false);
                Currency.Identifier selected = (Currency.Identifier) currencySpinner.getSelectedItem();
                MainActivity().goResult(Double.parseDouble(currencyAmount.getText().toString()), selected);
                break;
        }
    }


    @Override
    public void onErrorResponse(ValidOperation operation, Exception exception) {
        if (exception instanceof HandlerException){
            HandlerException error = (HandlerException) exception;
            MainActivity().toastWarning(error.getIcon(), error.getUserMessage(), error.getDuration());
        } else {
            MainActivity().toastWarning(R.drawable.ic_fatal_error, R.string.fatal_error, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == calculateButton){
            if (Utils.isNullOrEmpty(currencyAmount.getText().toString())) {
                onErrorResponse(ValidOperation.Login, new HandlerException(R.string.home_amount_empty));
            } else {
                homeViewModel.setLoading(true);
                Currency.getAll(this);
            }
        }
    }
}