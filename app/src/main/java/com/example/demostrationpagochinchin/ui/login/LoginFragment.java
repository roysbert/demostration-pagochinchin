package com.example.demostrationpagochinchin.ui.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.ActionCallback;
import com.example.demostrationpagochinchin.core.HandlerException;
import com.example.demostrationpagochinchin.core.Utils;
import com.example.demostrationpagochinchin.core.ValidOperation;
import com.example.demostrationpagochinchin.core.model.User;
import com.example.demostrationpagochinchin.ui.BaseFragment;

import static android.widget.Toast.LENGTH_LONG;

public class LoginFragment extends BaseFragment implements View.OnClickListener, ActionCallback {

    private EditText editUsername;
    private EditText editPassword;
    private Button loginButton;
    private TextView aboutText;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_login, container, false);

        editUsername = root.findViewById(R.id.login_username);
        editPassword = root.findViewById(R.id.login_password);

        loginButton = root.findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

        aboutText = root.findViewById(R.id.aboutButton);
        aboutText.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View view) {
        if (view == loginButton){
            if (Utils.isNullOrEmpty(editUsername.getText().toString())) {
                onErrorResponse(ValidOperation.Login, new HandlerException(R.string.user_empty_username));
            } else if (Utils.isNullOrEmpty(editPassword.getText().toString())) {
                onErrorResponse(ValidOperation.Login, new HandlerException(R.string.user_empty_password));
            } else {
                User.login(this, editUsername.getText().toString(), editPassword.getText().toString());
            }
        } else if (view == aboutText){
            MainActivity().toastInfo(R.drawable.ic_notifications_black_24dp, R.string.login_about_secret, LENGTH_LONG);
        }
    }

    @Override
    public void onSuccessResponse(ValidOperation operation) {
        switch (operation){
            case Login:
                MainActivity().goHome();
                break;
        }
    }

    @Override
    public void onErrorResponse(ValidOperation operation, Exception exception) {
        if (exception instanceof HandlerException){
            HandlerException error = (HandlerException) exception;
            MainActivity().toastWarning(error.getIcon(), error.getUserMessage(), error.getDuration());
        } else {
            MainActivity().toastWarning(R.drawable.ic_fatal_error, R.string.fatal_error, LENGTH_LONG);
        }
    }
}