package com.example.demostrationpagochinchin.ui.result;

import android.graphics.Bitmap;
import android.util.Base64;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.demostrationpagochinchin.HandlerApplication;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.core.model.Prediction;
import com.google.gson.Gson;

import java.util.ArrayList;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class ResultsViewModel extends ViewModel {

    private final MutableLiveData<Bitmap> qrImage;
    private Prediction prediction;
    private MutableLiveData<String> subTitle;
    private MutableLiveData<ArrayList<Currency>> currencies;

    public ResultsViewModel() {
        this.subTitle = new MutableLiveData<>();
        this.currencies = new MutableLiveData<>();
        this.qrImage = new MutableLiveData<>();

        prepareData();
    }

    private void prepareData(){

        prediction = HandlerApplication.shared().prediction();

        String template = "%.12f";
        if (prediction.getAmount() > 1)
            template = "%.2f";

        if (prediction.getSelectedCurrency().preValue()){
            subTitle.setValue(String.format("Al convertir %s%s según las diferentes tasas resulta:", prediction.getSelectedCurrency().unit(), String.format(template,prediction.getAmount())));
        } else {
            subTitle.setValue(String.format("Al convertir %s %s según las diferentes tasas resulta:", String.format(template,prediction.getAmount()), prediction.getSelectedCurrency().unit()));
        }
        ArrayList<Currency> currencies = HandlerApplication.shared().currencies();
        this.currencies.setValue(currencies);

        buildQR(prediction, currencies);
    }

    private void buildQR(Prediction prediction, ArrayList<Currency> currencies) {
        Gson gson = new Gson();
        String jsonCurrencies = gson.toJson(currencies);
        String jsonPrediction = gson.toJson(prediction);
        String encoded = null;
        try {
            String data = String.format("%s^%s", jsonCurrencies, jsonPrediction);
            encoded = new String(Base64.encode(data.getBytes(), Base64.DEFAULT), "UTF-8");
            QRGEncoder qrgEncoder = new QRGEncoder(encoded, null, QRGContents.Type.TEXT, 10);
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            qrImage.setValue(bitmap);
        } catch (Exception e) {
        }
    }

    public LiveData<String> subscribeSubtitle() {
        return subTitle;
    }

    public MutableLiveData<ArrayList<Currency>> subscribeCurrencies() {
        return currencies;
    }

    public LiveData<Bitmap> subscribeQR() {
        return qrImage;
    }

    public Prediction getPrediction() {
        return prediction;
    }
}