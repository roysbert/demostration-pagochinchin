package com.example.demostrationpagochinchin.ui.qr;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

import com.example.demostrationpagochinchin.HandlerApplication;
import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.core.model.Prediction;
import com.example.demostrationpagochinchin.ui.BaseFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class QRFragment extends BaseFragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_qr, container, false);

        IntentIntegrator scanIntegrator = IntentIntegrator.forSupportFragment(QRFragment.this);
        scanIntegrator.setPrompt("Scan");
        scanIntegrator.setBeepEnabled(true);
        scanIntegrator.setCaptureActivity(com.journeyapps.barcodescanner.CaptureActivity.class);
        scanIntegrator.setOrientationLocked(true);
        scanIntegrator.setBarcodeImageEnabled(true);
        scanIntegrator.initiateScan();
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanningResult != null) {
            try {
                String base64 = scanningResult.getContents();
                if (base64 == null)
                    throw new Exception("Base64 data is null");
                String decode = new String(Base64.decode(scanningResult.getContents(), Base64.DEFAULT),"UTF-8");
                String[] jsons = decode.split("\\^");

                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<Currency>>(){}.getType();
                ArrayList<Currency > currencies = gson.fromJson(jsons[0], type);
                Prediction prediction = gson.fromJson(jsons[1], Prediction.class);

                HandlerApplication.shared().saveCurrencies(currencies);
                MainActivity().goResult(prediction.getAmount(), prediction.getSelectedCurrency());
                return;
            } catch (Exception e) {
            }
        }
        MainActivity().goHome();
    }
}