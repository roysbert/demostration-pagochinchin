package com.example.demostrationpagochinchin.ui.result;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.ui.BaseFragment;

import java.util.ArrayList;

public class ResultsFragment extends BaseFragment {

    private ResultsViewModel resultsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        resultsViewModel =
                ViewModelProviders.of(this).get(ResultsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_result, container, false);
        final TextView subTitle = root.findViewById(R.id.result_subtitle);

        final ListView list = root.findViewById(R.id.result_list_currencies);
        list.setScrollContainer(false);
        resultsViewModel.subscribeSubtitle().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                subTitle.setText(s);
            }
        });
        final ResultsFragment self = this;
        resultsViewModel.subscribeCurrencies().observe(getViewLifecycleOwner(), new Observer<ArrayList<Currency>>() {
            @Override
            public void onChanged(ArrayList<Currency> currencies) {
                ResultCurrenciesAdapter adapter = new ResultCurrenciesAdapter(self.getContext(), currencies, resultsViewModel.getPrediction());
                list.setAdapter(adapter);

            }
        });

        final ImageView qrImageView = root.findViewById(R.id.result_qr);
        resultsViewModel.subscribeQR().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                qrImageView.setImageBitmap(bitmap);
            }
        });

        return root;
    }
}