package com.example.demostrationpagochinchin.ui;

import androidx.fragment.app.Fragment;

import com.example.demostrationpagochinchin.MainActivity;

public class BaseFragment extends Fragment {

    public MainActivity MainActivity(){
        if (getActivity() instanceof MainActivity)
            return (MainActivity) getActivity();
        throw new RuntimeException("The activity was expected to be MainActivity");
    }
}
