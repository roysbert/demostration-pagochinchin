package com.example.demostrationpagochinchin.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class HomeViewModel extends ViewModel {

    private MutableLiveData<Boolean> loading;

    public HomeViewModel() {
        loading = new MutableLiveData<>();
    }

    public void setLoading(Boolean value){
        loading.setValue(value);
    }

    public LiveData<Boolean> isLoading() {
        return loading;
    }

}