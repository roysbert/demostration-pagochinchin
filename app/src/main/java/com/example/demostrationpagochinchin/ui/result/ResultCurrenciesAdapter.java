package com.example.demostrationpagochinchin.ui.result;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.core.model.Prediction;

import java.util.List;

public class ResultCurrenciesAdapter extends BaseAdapter {

    private final List<Currency> currencies;
    private final Context context;
    private final Prediction prediction;
    private Currency selectedCurrency;

    public ResultCurrenciesAdapter(Context context, List<Currency> currencies, Prediction prediction){
        this.context = context;
        this.currencies = currencies;
        this.prediction = prediction;

        for (Currency currency: currencies) {
            if (currency.getId() == prediction.getSelectedCurrency()) {
                selectedCurrency = currency;
                break;
            }

        }
    }

    @Override
    public int getCount() {
        return currencies.size();
    }

    @Override
    public Object getItem(int i) {
        return currencies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return currencies.get(i).getId().ordinal();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.result_currency_item, null);
            viewHolder = new ViewHolder();
            viewHolder.label = view.findViewById(R.id.result_item_label);
            viewHolder.value = view.findViewById(R.id.result_item_value);
            viewHolder.tax = view.findViewById(R.id.result_item_tax);
            /**
             * At very first time when the List View row Item control's
             * instance is created it will be store in the convertView as a
             * ViewHolder Class object for the reusability purpose
             **/
            view.setTag(viewHolder);
        } else {
            /**
             * Once the instance of the row item's control it will use from
             * already created controls which are stored in convertView as a
             * ViewHolder Instance
             * */
            viewHolder = (ViewHolder) view.getTag();
        }

        Currency currency = currencies.get(i);
        Double value = prediction.getAmount() * selectedCurrency.getExchangeRate() / currency.getExchangeRate();

        viewHolder.label.setText(currency.getId().symbol());

        String template = "%.8f";
        if (value > 1.1)
            template = "%.2f";

        viewHolder.tax.setText(String.format("$%.2f", currency.getExchangeRate()));

        if (currency.getId().preValue()){
            viewHolder.value.setText(String.format("%s%s", currency.getId().unit(), String.format(template, value)));
        } else  {
            viewHolder.value.setText(String.format("%s %s", String.format(template, value), currency.getId().unit()));
        }


        return view;
    }

    public class ViewHolder {
        TextView label = null;
        TextView value = null;
        TextView tax = null;

    }
}
