package com.example.demostrationpagochinchin.core.model;

import androidx.annotation.NonNull;

import com.example.demostrationpagochinchin.HandlerApplication;
import com.example.demostrationpagochinchin.core.ActionCallback;

/**
 * The purpose of this class is to store and manage the relevant currency information in the app.
 */
public class Currency {

    public Currency(Identifier id) {
        this.id = id;
        this.exchangeRate = id.defaultExchangeRate();
    }

    public static void getAll(ActionCallback action){
        HandlerApplication.shared().serviceManager().fetchCurrencies(action);
    }

    /**
     * This numeric defines the valid identifiers for the currencies supported by the app.
     */
    public enum Identifier {
        USD,
        ETH,
        BTC,
        PTR,
        BS,
        EURO;

        /**
         * This function allows to obtain from an identifier the symbol that represents it.
         * @return I return a string representing the symbol for this identifyReturn a string representing the symbol for this identify.
         */
        public String symbol() {
            switch (this) {
                case ETH:
                    return "ETHTUSD";
                case BTC:
                    return "BTCTUSD";
                case EURO:
                    return "EURBUSD";
                case BS:
                    return "VEFTUSD";
                case PTR:
                    return "PTRTUSD";
                case USD:
                    return "USDTUSD";

                default:
                    return "???";
            }

        }


        /**
         * This function allows to obtain from an identifier the symbol that represents it.
         * @return I return a string representing the symbol for this identifyReturn a string representing the symbol for this identify. $1000
         */
        public String unit() {
            switch (this) {
                case USD:
                    return "$";
                case ETH:
                    return "ETH";
                case BTC:
                    return "BTC";
                case PTR:
                    return "PTR";
                case EURO:
                    return "EURO";
                case BS:
                    return "Bs";
                default:
                    return "???";
            }

        }

        /**
         * This function indicates whether the symbol should be placed before the value or not.
         * @return This is the response after querying the identifier.
         */
        public boolean preValue() {
            switch (this) {
                case USD:
                    return true;
                case ETH:
                    return false;
                case BTC:
                    return false;
                case PTR:
                    return false;
                case EURO:
                    return false;
                case BS:
                    return false;
                default:
                    return false;
            }

        }

        public Double defaultExchangeRate() {
            switch (this) {
                case USD:
                    return 1.0d;
                case PTR:
                    return 60.0d;
                case BS:
                    return 0.00001d;
                default:
                    return 0.0d;
            }

        }

        @NonNull
        @Override
        public String toString() {
            return String.format("%s - %s", symbol(), unit());
        }
    }

    /**
     * attribute allows to guarantee the uniqueness of a currency.
     */
    private Identifier id;

    /**
     * attribute allows to determine the value of a currency with respect to the dollar.
     */
    private Double exchangeRate;


    public Currency(Identifier id, Double exchangeRate) {
        this.id = id;
        this.exchangeRate = exchangeRate;
    }

    public Identifier getId() {
        return id;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("%s - %s", getId().symbol(), getId().unit());
    }
}
