package com.example.demostrationpagochinchin.core.model;

import android.app.Application;
import android.content.OperationApplicationException;
import android.util.Log;
import android.widget.Toast;

import com.example.demostrationpagochinchin.HandlerApplication;
import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.ActionCallback;
import com.example.demostrationpagochinchin.core.HandlerException;
import com.example.demostrationpagochinchin.core.Utils;
import com.example.demostrationpagochinchin.core.ValidOperation;

import org.json.JSONObject;

/**
 * This class allows to store and manage the relevant information of a user in the application.
 */
public class User {

    /**
     * This function emulates the invocation of a dedicated service with a fixed user and password.
     * @param username
     * @param password
     * @return
     */
    public static void login(ActionCallback action, String username, String password) {
        String hash = Utils.getSHA512(password);        
        if (username.equals("roysbert.salinas") && hash.equals("dc3023bbe5af46f0874ff04e7d6171cb724aec5e28be127ca06d9debe99059e313ccbb8a29b72a77206c5939c3b0ac2f6457aa9705c14ea534d9489316f8d697")){
            String token = Utils.getSHA512("valid user");
            HandlerApplication.shared().saveCurrentUser(new User(username, token));
            action.onSuccessResponse(ValidOperation.Login);
        } else {
            action.onErrorResponse(ValidOperation.Login, new HandlerException(R.string.user_invalid, "De momento el uníco usuario valido es roysbert,salinas y su clave es rs123456."));
        }
    }

    /**
     * This constructor allows to establish a user instance from the password name, it is only accessible from this same class.
     * @param username Is the user name that logs in.
     * @param token Is the access token towards the system service.
     */
    private User(String username, String token){
        this.username = username;
        this.token = token;
    }

    /**
     * attribute containing the name of the user to log in to the system.
     */
    private String username;

    /**
     * attribute containing the token in sha512 format of the user to log into the system.
     */
    private String token;

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }
}
