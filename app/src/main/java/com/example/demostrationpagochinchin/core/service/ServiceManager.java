package com.example.demostrationpagochinchin.core.service;

import android.content.Context;
import android.util.Log;

import com.example.demostrationpagochinchin.HandlerApplication;
import com.example.demostrationpagochinchin.R;
import com.example.demostrationpagochinchin.core.ActionCallback;
import com.example.demostrationpagochinchin.core.HandlerException;
import com.example.demostrationpagochinchin.core.ValidOperation;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.core.service.prototype.BinanceCurrency;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceManager {

    private Retrofit binanceClient;

    public ServiceManager(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();
        this.binanceClient = new Retrofit.Builder()
                .baseUrl("https://api.binance.com")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void fetchCurrencies(final ActionCallback action) {
        ///
        BinanceEndPoint endPoint = binanceClient.create(BinanceEndPoint.class);
        endPoint.GetCurrencies().enqueue(new Callback<List<BinanceCurrency>>() {
            @Override
            public void onResponse(Call<List<BinanceCurrency>> call, Response<List<BinanceCurrency>> response) {
                Currency.Identifier[] ids = {
                        Currency.Identifier.BTC,
                        Currency.Identifier.ETH,
                        Currency.Identifier.EURO,
                };
                ArrayList<Currency> currencies = new ArrayList<>();
                if (response.isSuccessful()){
                    for (Currency.Identifier currentId : ids) {
                        boolean added = false;
                        for (BinanceCurrency currency: response.body()) {
                            if (currency.getSymbol().equals(currentId.symbol())){
                                added = true;
                                currencies.add(new Currency(currentId, Double.parseDouble(currency.getPrice())));
                                break;
                            }
                        }
                        if (!added) {
                            currencies.add(new Currency(currentId));
                        }
                    }
                    currencies.add(new Currency(Currency.Identifier.BS));
                    currencies.add(new Currency(Currency.Identifier.PTR));
                    currencies.add(new Currency(Currency.Identifier.USD));
                    HandlerApplication.shared().saveCurrencies(currencies);
                    action.onSuccessResponse(ValidOperation.BinanceCurrencies);
                } else {
                    action.onErrorResponse(ValidOperation.BinanceCurrencies, new HandlerException(R.string.fatal_error, "The service response is not as expected.."));
                }
            }

            @Override
            public void onFailure(Call<List<BinanceCurrency>> call, Throwable t) {
                action.onErrorResponse(ValidOperation.BinanceCurrencies, new HandlerException(R.string.fatal_error, "The service response is not as expected.."));
            }
        });
    }
}
