package com.example.demostrationpagochinchin.core;

/**
 * This interface is used to control actions after an asynchronous process.
 */
public interface ActionCallback {
    /**
     * This function is invoked after the success of an asynchronous process, this process must control how to share the resulting data.
     * @param operation This is the operation that reports that it finished correctly.
     */
    void onSuccessResponse(ValidOperation operation);

    /**
     * This function is invoked after some error of an asynchronous process, this process returns some exception related to the problem.
     * @param operation This is the operation you report that ended with an error.
     * @param exception Exception to the problem.
     */
    void onErrorResponse(ValidOperation operation, Exception exception);
}
