package com.example.demostrationpagochinchin.core.service;

import com.example.demostrationpagochinchin.core.service.prototype.BinanceCurrency;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface BinanceEndPoint {

    @Headers({
            "Content-Type: application/json"
    })

    @GET("/api/v3/ticker/price")
    Call<List<BinanceCurrency>> GetCurrencies();

}
