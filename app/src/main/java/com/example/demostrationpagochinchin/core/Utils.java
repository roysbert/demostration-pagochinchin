package com.example.demostrationpagochinchin.core;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * this class has definitions that allow it to be used in the global scope of the application.
 */
public class Utils {

    public static String getSHA512(String message) {
        MessageDigest md = null;
        StringBuilder sb = new StringBuilder();
        try {
            md = MessageDigest.getInstance("SHA-512");
            byte[] digest = md.digest(message.getBytes());
            for (int i = 0; i < digest.length; i++) {
                sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static boolean isNullOrEmpty(Object data) {
        if (data instanceof String){
            String s = (String) data;
            return s.equals("");
        }
        return data == null;
    }
}
