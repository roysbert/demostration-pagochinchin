package com.example.demostrationpagochinchin.core;

import android.widget.Toast;

/**
 * This class allows you to control errors that can be displayed on the screen, using resources such as icon, message and duration.
 */
public class HandlerException extends Exception {

    private int refUserMessage;
    private int refIcon;
    private int refDuration;

    public HandlerException(int refIcon, int refUserMessage, String descriptionMessage, int refDuration){
        super(descriptionMessage);
        this.refUserMessage = refUserMessage;
        this.refIcon = refIcon;
        this.refDuration = refDuration;
    }

    public HandlerException(int refIcon, int refUserMessage, String descriptionMessage){
        super(descriptionMessage);
        this.refUserMessage = refUserMessage;
        this.refIcon = refIcon;
        this.refDuration = Toast.LENGTH_SHORT;
    }

    public HandlerException(int refIcon, int refUserMessage){
        super();
        this.refUserMessage = refUserMessage;
        this.refIcon = refIcon;
        this.refDuration = Toast.LENGTH_SHORT;
    }

    public HandlerException(int refUserMessage){
        super();
        this.refUserMessage = refUserMessage;
        this.refIcon = -1;
        this.refDuration = Toast.LENGTH_SHORT;
    }

    public HandlerException(int refUserMessage, String descriptionMessage, int refDuration){
        super(descriptionMessage);
        this.refUserMessage = refUserMessage;
        this.refIcon = -1;
        this.refDuration = refDuration;
    }

    public HandlerException(int refUserMessage, String descriptionMessage){
        super(descriptionMessage);
        this.refUserMessage = refUserMessage;
        this.refIcon = -1;
        this.refDuration = Toast.LENGTH_SHORT;
    }

    public int getUserMessage() {
        return this.refUserMessage;
    }

    public int getIcon() {
        return this.refIcon;
    }

    public int getDuration() {
        return this.refDuration;
    }
}
