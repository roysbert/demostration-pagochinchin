package com.example.demostrationpagochinchin.core.model;

/**
 * This class is used to control the information needed to make a prediction.
 */
public class Prediction {

    /**
     * It is the amount to calculate for each selectedCurrency.
     */
    private Double amount;

    /**
     * It is the unit of the amount to be calculated
     */
    private Currency.Identifier selectedCurrency;

    /**
     * This constructor allows you to create a prediction instance with the necessary information.
     * @param amount This is the value that the user indicates to calculate.
     * @param selectedCurrency This is the currency specified by the user associated with the amount.
     */
    public Prediction(Double amount, Currency.Identifier selectedCurrency) {
        this.amount = amount;
        this.selectedCurrency = selectedCurrency;
    }

    public Double getAmount() {
        return amount;
    }

    public Currency.Identifier getSelectedCurrency() {
        return selectedCurrency;
    }

}
