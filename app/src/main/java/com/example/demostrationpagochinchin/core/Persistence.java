package com.example.demostrationpagochinchin.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.core.model.Prediction;
import com.example.demostrationpagochinchin.core.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * This class controls the persistence of the application, here are defined the read and write methods of the data structures used in the application.
 */
public class Persistence {

    public class Keys {
        public static final String currentUser = "currentUser";
        public static final String currencies = "currencies";
        public static final String prediction = "prediction";
    }

    private SharedPreferences sharedPreferences;

    public Persistence(Context context) {
        this.sharedPreferences = context.getApplicationContext().getSharedPreferences("DemoAppDB", 0);
    }

    public User rescueCurrentUser(){
        String data = sharedPreferences.getString(Keys.currentUser,"");
        if (!data.equals("")) {
            Gson gson = new Gson();
            return gson.fromJson(data, User.class);
        }
        return null;
    }

    public void saveCurrentUser(User user) {
        SharedPreferences.Editor transaction = sharedPreferences.edit();
        Gson gson = new Gson();
        String data = gson.toJson(user);
        transaction.putString(Keys.currentUser, data);
        transaction.apply();
    }


    public void clearCurrentUser() {
        SharedPreferences.Editor transaction = sharedPreferences.edit();
        transaction.remove(Keys.currentUser);
        transaction.apply();
    }


    public ArrayList<Currency> rescueCurrencies(){
        String data = sharedPreferences.getString(Keys.currencies,"");
        if (!data.equals("")) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Currency>>(){}.getType();
            return gson.fromJson(data, type);
        }
        return null;
    }

    public void saveCurrencies(ArrayList<Currency> currencies) {
        SharedPreferences.Editor transaction = sharedPreferences.edit();
        Gson gson = new Gson();
        String data = gson.toJson(currencies);
        transaction.putString(Keys.currencies, data);
        transaction.apply();
    }

    public void savePrediction(Prediction prediction) {
        SharedPreferences.Editor transaction = sharedPreferences.edit();
        Gson gson = new Gson();
        String data = gson.toJson(prediction);
        transaction.putString(Keys.prediction, data);
        transaction.apply();
    }

    public Prediction rescuePrediction(){
        String data = sharedPreferences.getString(Keys.prediction,"");
        if (!data.equals("")) {
            Gson gson = new Gson();
            return gson.fromJson(data, Prediction.class);
        }
        return null;
    }
}
