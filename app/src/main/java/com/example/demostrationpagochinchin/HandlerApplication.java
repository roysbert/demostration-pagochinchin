package com.example.demostrationpagochinchin;

import android.app.Application;
import android.content.Context;

import com.example.demostrationpagochinchin.core.Persistence;
import com.example.demostrationpagochinchin.core.model.Currency;
import com.example.demostrationpagochinchin.core.model.Prediction;
import com.example.demostrationpagochinchin.core.service.ServiceManager;
import com.example.demostrationpagochinchin.core.model.User;

import java.util.ArrayList;

public class HandlerApplication extends Application  {

    private static HandlerApplication currentInstance;

    public static synchronized HandlerApplication shared(Context context) {
        if (currentInstance == null && context != null){
            currentInstance = (HandlerApplication) context.getApplicationContext();
        }
        return currentInstance;
    }

    public static HandlerApplication shared() {
        return currentInstance;
    }

    private Persistence persistence = null;

    public Persistence persistence() {
        if (persistence == null) {
            persistence = new Persistence(getApplicationContext());
        }
        return persistence;
    }

    private ServiceManager serviceManager;

    private ArrayList<Currency> currencies;
    private Prediction prediction;
    private User currentUser;

    public HandlerApplication(){
        super();
        currentInstance = this;
        currentUser = null;
        prediction = null;
        currencies = null;
    }

    public User currentUser(){
        if (currentUser == null){
            currentUser = persistence().rescueCurrentUser();
        }
        return currentUser;
    }

    public void logout() {
        persistence().clearCurrentUser();
        currentUser = null;
    }

    public void saveCurrentUser(User user) {
        persistence().saveCurrentUser(user);
        currentUser = user;
    }

    public ServiceManager serviceManager() {
        if (serviceManager == null){
            serviceManager = new ServiceManager(getApplicationContext());
        }

        return serviceManager;
    }

    public ArrayList<Currency> currencies() {
        if (currencies == null){
            currencies = persistence().rescueCurrencies();
        }
        return currencies;
    }

    public void saveCurrencies(ArrayList<Currency> currencies) {
        persistence().saveCurrencies(currencies);
        this.currencies = currencies;
    }

    public Prediction prediction(){
        prediction = persistence().rescuePrediction();
        return prediction;
    }

    public void savePrediction(Double amount, Currency.Identifier currency) {
        this.prediction = new Prediction(amount, currency);
        persistence().savePrediction(this.prediction);
    }

}
