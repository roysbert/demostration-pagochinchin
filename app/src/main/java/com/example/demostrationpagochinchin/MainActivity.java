package com.example.demostrationpagochinchin;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.demostrationpagochinchin.core.model.Currency;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavView;
    private NavController navController;
    private final int MenuItemSummaryId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.bottom_navigation_home,
                R.id.bottom_navigation_qr
        ).build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(bottomNavView, navController);

        reload();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        new MenuInflater(this).inflate(R.menu.nav_menu, menu);
        if (HandlerApplication.shared().prediction() != null) {
            MenuItem edit_item = menu.add(0, MenuItemSummaryId, 0, R.string.previously);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.navigation_logout){
            navController.navigate(R.id.navigation_logout);
        } else if (item.getItemId() == MenuItemSummaryId){
            navController.navigate(R.id.navigation_result);
        }
        return super.onOptionsItemSelected(item);
    }

    public void reload() {
        if (HandlerApplication.shared(this).currentUser() == null){
            goLogin();
        } else {
            goHome();
        }
    }

    public void goBack() {
        navController.popBackStack();
        bottomNavView.setVisibility(View.VISIBLE);
    }

    public void hideBottomNavigationBar() {
        bottomNavView.setVisibility(View.GONE);
    }

    public void goLogin() {
        getSupportActionBar().hide();
        navController.popBackStack(R.id.navigation_login, true);
        navController.navigate(R.id.navigation_login);
        bottomNavView.setVisibility(View.GONE);
    }

    public void goResult(Double amount, Currency.Identifier currency){
        HandlerApplication.shared().savePrediction(amount, currency);
        navController.navigate(R.id.navigation_result);
    }

    public void goHome() {
        getSupportActionBar().show();
        navController.popBackStack(R.id.bottom_navigation_home, true);
        navController.navigate(R.id.bottom_navigation_home);
        bottomNavView.setVisibility(View.VISIBLE);
    }


    private enum ToastType {
        Error,
        Success,
        Info,
        Warning
    }

    private void baseToastMessage(ToastType type, int refIcon, int refMessage, int refDuration){
        Toast.makeText(this, refMessage, refDuration).show();
    }

    public void toastError(int refIcon, int refMessage, int refDuration){
        baseToastMessage(ToastType.Error, refIcon, refMessage, refDuration);
    }

    public void toastInfo(int refIcon, int refMessage, int refDuration){
        baseToastMessage(ToastType.Info, refIcon, refMessage, refDuration);
    }

    public void toastSuccess(int refIcon, int refMessage, int refDuration){
        baseToastMessage(ToastType.Success, refIcon, refMessage, refDuration);
    }

    public void toastWarning(int refIcon, int refMessage, int refDuration){
        baseToastMessage(ToastType.Warning, refIcon, refMessage, refDuration);
    }

    @Override
    public void onBackPressed() {
        // disabled
        //super.onBackPressed();
    }
}
